% Figure 6c, checking stability of the LF consensus protocol (in terms of
% k and a)
clear
clf
close all

n = 100;% number of agents
m = 4;% number of outgoing connections by active agents
eps = 0.3;% persuasibility parameter
K = linspace(0.01,1,70);% scale for the number of followers and leaders
A = linspace(0,1,40);% average activity

Z = zeros(length(A),length(K));
Conv = zeros(length(A),length(K));

for i = 1:length(A)    
    for j = 1:length(K)        
        Z(i,j) = lambda6(eps, m , A(i), n, K(j));        
    end    
end

for i = 1:length(A)    
    for j = 1:length(K)        
        if Z(i,j) < .98
            Conv(i,j) = Z(i,j);
        else
            Conv(i,j) = 1;            
        end        
    end
end

h=pcolor(K,A,log(Conv));
colormap('Hot')
xlabel(' k')
ylabel('a')
set(h, 'EdgeColor', 'none');
colorbar

% consentability threshold 
Ci = log(Conv);
hold on
hh= contour(K,A,Ci, [0 0],'b','LineWidth',2);% blue line to indicate consentability threshold
hold off