% Figure 5b, checking stability of the LF consensus protocol (in terms of
% m and eps)
clear
clf
close all

n = 100;% number of agents
k = .5;% scale for the number of followers and leaders
a = .4;% average activity
eps = linspace(0.001,0.4,50);% persuasibility parameter
M = linspace(1,20,20); % number of outgoing connections by active agents

Z = zeros(length(eps),length(M));
Conv = zeros(length(eps),length(M));

for i = 1:length(eps)
    for j = 1:length(M)
        Z(i,j) = lambda6(eps(i), M(j), a, n, k);
    end
end% compute matrix with lambda_6 entries for each eps and m

for i = 1:length(eps)
    for j = 1:length(M)
        if Z(i,j) < 1
            Conv(i,j) = Z(i,j);
        else
            Conv(i,j) = 1;
        end
    end
end% create matrix for heat map

h=pcolor(M,eps,log(Conv));
colormap('Hot')
xlabel(' m')
ylabel('\epsilon')
set(h, 'EdgeColor', 'none');

% plot consentability threshold
hold on
Ci = log(Conv);
hh= contour(M,eps,Ci, [0 0],'b','LineWidth',2);% black line to indicate consentability threshold
hold off
colorbar