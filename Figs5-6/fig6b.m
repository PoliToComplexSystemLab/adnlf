% Figure 6b, checking stability of the LF consensus protocol (in terms of
% a and eps)
clear
clf
close all

n = 100;% number of agents
m = 4;% number of outgoing connections by active agents
k = .8;% scale for the number of followers and leaders
eps = linspace(0,.6,50);% persuasibility parameter
A = linspace(0,1,50);% average activity

Z = zeros(length(eps),length(A));
Conv = zeros(length(eps),length(A));

for i = 1:length(eps)    
    for j = 1:length(A)        
        Z(i,j) = lambda6(eps(i), m, A(j), n, k);        
    end    
end

for i = 1:length(eps)    
    for j = 1:length(A)        
        if Z(i,j) < 1
            Conv(i,j) = Z(i,j);
        else
            Conv(i,j) = 1;            
        end        
    end
end

h=pcolor(A,eps,log(Conv));
colormap('Hot')
xlabel(' a')
ylabel('\epsilon')
set(h, 'EdgeColor', 'none');
colorbar

% consentability threshold 
Ci = log(Conv);
hold on
hh= contour(A,eps,Ci, [0 0],'b','LineWidth',2);% blue line to indicate consentability threshold
hold off