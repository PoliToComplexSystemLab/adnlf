% Figure 6a, checking stability of the LF consensus protocol (in terms of
% k and eps)
clear
clf
close all

n = 100;% number of agents
m = 4;% number of outgoing connections by active agents
a = .3;% average activity
eps = linspace(0.0001,.6,50);% persuasibility parameter
K = linspace(0.0001,1,49);% scale for the number of followers and leaders

Z = zeros(length(eps),length(K));
Conv = zeros(length(eps),length(K));

for i = 1:length(eps)
    for j = 1:length(K)        
        Z(i,j) = lambda6(eps(i), m, a, n, K(j));        
    end
end% compute matrix with lambda_6 entries for each eps and m
for i = 1:length(eps)
    for j = 1:length(K)-1        
        if Z(i,j) < .981
            Conv(i,j) = Z(i,j);
        else
            Conv(i,j) = 1;            
        end
    end
end% create matrix for heat map

h=pcolor(K,eps,log(Conv));
 colormap('Hot')
set(h,'EdgeColor', 'none');

% consentability threshold 
Ci = log(Conv);
hold on
hh= contour(K(1:end-1),eps(1:end-1),Ci(1:49,1:48), [0 0],'b','LineWidth',3);% blue line to indicate consentability threshold
hold off

ylim([0 .55])
xlabel(' k')
ylabel('\epsilon')
colorbar
