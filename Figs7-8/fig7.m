% Figure 7, comparing homogeneous and heterogenous ADNs
clf
close all
clear all

n = 100;% number of nodes
k = .5;% scale for the number of followers and leaders
m = 4;% number of outgoing connections by active agents
sigma = 0.35;% standard deviation of activity potential
alpha = 1;% sum of the follower deviations from the average activity potential 
eps = linspace(0.01,.6,80);% persuasibility parameter
A = linspace(0.01,1,80);% average activity


rHET = zeros(length(A),length(eps));
rHOM = zeros(length(A),length(eps));

for i = 1:length(A)    
    for j=1:length(eps)        
        rHET(i,j) = perturbation(eps(j), m, A(i), n, k, alpha, sigma);% r of heterogeneous ADN
        rHOM(i,j) = lambda6(eps(j), m, A(i), n, k);% r of homogeneous ADN                
    end    
end

Diff = (rHET-rHOM)./rHOM;
Diff(Diff>0) = 0;


h=pcolor(A,eps,Diff);
colormap('Hot')
xlabel('a')
ylabel('\epsilon')
set(h, 'EdgeColor', 'none');
colorbar

Z = zeros(length(eps),length(A));
Conv = zeros(length(eps),length(A));
for i = 1:length(eps)
    
    for j = 1:length(A)
        
        Z(i,j) = lambda6(eps(i), m, A(j), n, k);
        
    end
    
end
for i = 1:length(eps)    
    for j = 1:length(A)        
        if Z(i,j) < 1
            Conv(i,j) = Z(i,j);
        else
            Conv(i,j) = 1;            
        end        
    end
end% used for consentability threshold

hold on
contour(A,eps,log(Conv), [0 0],'b','LineWidth',2);% blue line to indicate consentability threshold
hold off