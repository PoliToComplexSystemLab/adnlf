function r = perturbation(eps, m, a, n, k, alpha, sigma )
% computing the asymptotic convergence factor, r, from Theorem 3.2

% eps = persuasibility parameter
% m = number of outgoing connections by active agents
% a = average activity
% n = number of nodes
% k = scale for the number of followers and leaders
% alpha = sum of the follower deviations from the average activity potential 
% sigma = standard deviation of activity potential

f = k*n; 
l = (1-k)*n;

q = eps*m/(n-1);

A = eps*(f-1) + eps*(m-1)*(f-1)*(f-2)/(n-2) + q*(n-1)*(l - f + 1) - q*a*l^2;
B = eps*(f-1) - 2*f + q*(n-1)^2 + q*a*(n+l-l^2);
C = q*a*(n+l)-2;
gamma6 = ( -B + sqrt( B^2 - 4*A*C ) )/( 2*A );% compute gamma_6

lambda6 = (1-l*q*a)^2 + (2*q*a - (n+l)*q^2*a^2)/gamma6;% compute lambda_6

r1 = (gamma6^2*(-2*q^2*l^2*a*(f-1) - q*l*f ...
    + q^2*(n-1)*(l-f+1)+ eps*q*(m-1)*(f-1)*(f-2)/(n-2) + eps*q*(f-1)) ...
    + gamma6*(2*q*(l*f-n-l) + 2*q^2*(n-1)*l + eps*q*(m-1)*(f-1)*(f-2)/(n-2) ...
    - 2*q^2*a*(n+l)*(l*f-1) + 2*(f-1)*eps*q) ...
    - 2*q^2*l*a*(n+l)*(l-1)-2*q*f + q^2*(n-1)^2 ...
    + (f-1)*eps*q)/(f^2*gamma6^2 + 2*f*gamma6 + f); % compute rho_1

r = lambda6 + sigma*alpha*r1; 


end

