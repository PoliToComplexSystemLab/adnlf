function lambda = lambda6(eps, m, a, n, k)
% computing lambda6 from Theorem 3.1

% eps = persuasibility parameter
% m = number of outgoing connections by active agents
% a = average activity
% n = number of nodes
% k = scale for number of followers and leaders

f = k*n; 
l = (1-k)*n;

q = eps*m/(n-1);

A = eps*(f-1) + eps*(m-1)*(f-1)*(f-2)/(n-2) + q*(n-1)*(l - f + 1) - q*a*l^2;
B = eps*(f-1) - 2*f + q*(n-1)^2 + q*a*(n+l-l^2);
C = q*a*(n+l)-2;
gamma6 = (-B + sqrt(B^2 - 4*A*C))/(2*A);% compute gamma_6

lambda = (1-l*q*a)^2 + (2*q*a - (n+l)*q^2*a^2)/gamma6;% compute lambda_6

end

