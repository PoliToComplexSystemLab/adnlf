% Figure 8b, comparing homogeneous and heterogenous ADNs
% note: change the index variable for plots (a) and (b)
clear
clf
close all

n = 100; % number of agents
k = .65;% ratio of leaders to followers:  followers = k*N and leaders = (1-k)*N
a = .25; %average activity
sigma = 0.2;%std of activity
alpha = 1;%sum of deviations from the std of heterogeneity
eps = linspace(0.001,0.4,50);% weight parameters
M = linspace(1,20,20);%number of connections created by active agents

Z = zeros(length(eps),length(M));
Conv = zeros(length(eps),length(M));
rHET = zeros(length(M),length(eps));
rHOM = zeros(length(M),length(eps));

for i = 1:length(eps)
    
    for j = 1:length(M)
        
        Z(i,j) = lambda6(eps(i), M(j), a, n, k);
        
    end
    
end
for i = 1:length(eps)    
    for j = 1:length(M)        
        if Z(i,j) < 1
            Conv(i,j) = Z(i,j);
        else
            Conv(i,j) = 1;            
        end        
    end
end% used for consentability threshold


for i = 1:length(M)    
    for j=1:length(eps)        
        rHET(i,j) = perturbation(eps(j), M(i), a, n, k, alpha, sigma );% r of heterogeneous
        rHOM(i,j) = lambda6(eps(j), M(i), a, n, k);% r of NCN        
    end    
end% compute r for heterogenous ADN and NCN

Diff = (rHET-rHOM)./rHOM;


colormap('Hot')
h = pcolor(M,eps,Diff');
xlabel('m')
ylabel('\epsilon')
colorbar
set(h, 'EdgeColor', 'none');

% consentability threshold 
hold on
contour(M,eps,log(Conv), [0 0],'b','LineWidth',2);% blue line to indicate consentability threshold
hold off