function [dis,x] = consensusLF_ADN(N,f,a,m,eps,x0,s,T)
%consensusLF computes T steps of consensus algorithm on ADN with N nodes (f followers), activity potential a, and initial condition x0. Realized by Zino.
    if length(a)==1
        a=a*ones(N,1);
    end
    x=zeros(f,T+1);
    x(:,1)=x0;
    for t=1:T
        A=dADN(N,m,a);
        P=eye(N)-eps*(diag(sum(A,2))-A);
        temp=P*[x(:,t); ones(N-f,1)*s];
        x(:,t+1)=temp(1:f);
    end
    dis=sqrt(sum((x-s).^2));
end

