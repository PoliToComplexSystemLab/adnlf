n=60; %number of nodes
f=45; %number of followers
m=4; %number of connections created by active agents
a=.3; %average activity
eps=.2;
s=.5;
T=200;
x=rand(1,f);
s=mean(x);
R=200;
for i=1:R
prova(i,:)=consensusLF_ADN(n,f,a,m,eps,x,s,T);
end

[aa,bb]=eig(H0lf(n,f,m,a,eps));
u=aa(:,1);
u=u/(u(end)-u(end-1));
gamma=u(end-1);
u=aa(:,2);
u=u/(u(end)-u(end-1));
gamma5=u(end-1);
[aa,bb]=eig(H0lf(n,f,m,a,eps)');
u=aa(:,1);
u=u/(u(end)-u(end-1));
omega=u(end-1);
u=aa(:,2);
u=u/(u(end)-u(end-1));
omega5=u(end-1);

c6=(1+gamma)*(norm(x-s)^2+omega*sum(x-s)^2)/(1+gamma+omega+gamma*omega);
c5=(1+gamma5)*(norm(x-s)^2+omega5*sum(x-s)^2)/(1+gamma5+omega5+gamma5*omega5);
xi=(c5+c6*0.95)*bb(2,2).^(0:T)+(c6-c6*0.95)*bb(1,1).^(0:T);

l = n-f;
q = eps*m/(n-1);

A = eps*(f-1) + eps*(m-1)*(f-1)*(f-2)/(n-2) + q*(n-1)*(l-f+1) - q*a*l^2;
B = eps*(f-1) - 2*f + q*(n-1)^2 + q*a*(n+l-l^2);
C = q*a*(n + l) - 2;
g = ( -B + sqrt( B^2 - 4*A*C ) )/( 2*A );
r=(1-l*q*a)^2+(2*q*a-(n+l)*q^2*a^2)/g;


figure
hold on
errorbar(0:T,log(mean(prova)),2.56*sqrt(var(log(prova)))/sqrt(R),'r')
plot(0:T,-log(sqrt(xi(1)))+log(prova(1))+log(sqrt(xi)));

y=log(mean(prova))
real=bb(1)
polyfit(ceil(T/2-1):T,y(ceil(T/2):201),1);
estim=exp(ans(1)).^2