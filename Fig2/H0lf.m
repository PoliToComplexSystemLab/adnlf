function H=H0lf(n,f,m,a,eps)
% Computes the second order consensus matrix for a leader-follower
% consensus system with n agents, f followers, m links created by each node
% with activity a and averaging parameter eps
    H=zeros(f^2);
     H=zeros(f^2);
    q=eps*m/(n-1);
    for i=1:f
        v=zeros(f,1);
        v(i)=1;
        H((i-1)*f+1:f*i,(i-1)*f+1:f*i)=q*a*(1-q*(n-1)*a)*ones(f)+q^2*a*(n-1)*(a-1)*v*ones(1,f)+(1-q*a*(2*n-1)+q^2*a^2*(n-1)*n)*eye(f)+q^2*(n-1)*n*a*(1-a)*v*v';    
    end
        %H((i-1)*f+1:f*i,(i-1)*f+1:f*i)=(eps*m*a/(n-1)-eps^2*m^2*a^2/(n-1))*(ones(f)-v*ones(1,f)-eye(f)+v*v')+(1-2*m*eps*a+eps^2*m^2*a^2)*(eye(f)-v*v')+(1-2*m*eps*a+eps^2*m^2*a)*v*v'+(eps*m*a/(n-1)-eps^2*m^2*a/(n-1))*(v*ones(1,f)-v*v');
    for i=1:f
        for j=1:f
            if i~=j
                v=zeros(f,1);
                v(i)=1;
                u=zeros(f,1);
                u(j)=1;
                H((i-1)*f+1:f*i,(j-1)*f+1:f*j)=q^2*a^2*ones(f)+q*a*(eps*(m-1)/(n-2)-q*a)*v*ones(1,f)+q*a*(1-q*a*n)*eye(f)+q*a*(q*a*n-q*(n-1)-eps*(m-1)/(n-2))*v*v'+eps*q*a*(1-(m-1)/(n-2))*v*u';
                %H((i-1)*f+1:f*i,(j-1)*f+1:f*j)=(eps^2*m^2*a^2/(n-1)^2)*(ones(f)-v*ones(1,f)-eye(f)+v*v')+(eps*m*a/(n-1)-eps^2*m^2*a^2/(n-1))*(eye(f)-v*v')+(eps^2*m*(m-1)*a/((n-1)*(n-2)))*(v*ones(1,f)-v*v'-v*u')+eps^2*m*a/(n-1)*v*u'+(eps*m*a/(n-1)-eps^2*m^2*a/(n-1))*v*v';
            end
        end
    end
end