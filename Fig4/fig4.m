% Figure 4c, checking asymptotic convergence factor for large N
clear
clf
close all

N=1000;% number of agents
k = .5;% scale for number of followers and leaders
m=4; % number of outgoing connections by active agents
a=.3; % average activity
eps=0.4;% persuasibility parameter
sigma = 0.3;% standard deviation of activity potential
alpha = 1; % sum of the follower deviations from the average activity potential 

% computing the asymptotic convergence factor, ar
if eps > 2*k/(k+m-m*a*(1-k)^2)
    ar = (1-eps*m*a*(1-k))^2 + eps*m*a*(eps*k -2*k + eps*m - eps*m*a*(1-k)^2)...
        -2*sigma*alpha*(eps*m)^2*a*(1-k)^2*(2-k)/k;
elseif eps <= 2*k/(k+m-m*a*(1-k)^2)
    ar = (1-eps*m*a*(1-k))^2;
end

plot([0 N],[ar ar],'k')% plot asymptotic value
NN = linspace(3,N,500);
r = zeros(1,length(NN));

% computing convergence factor, r, from Theorem 3.2
for i = 1:length(NN)    
    r(i) = perturbation(eps, m, a, NN(i), k, alpha, sigma );
end

hold on
plot(NN,r, 'r')
xlabel(' n')
ylabel('\rho ')

