function m = modn(a, n)
%modn(a, n) computes a mod n but instead of 0 in uses n (ex modn(4,2)=2)
m=mod(a,n);
m(m==0)=n;
end
