n=18; %number of nodes
f=12; %number of followers
m=3; %number of connections created by active agents
a=.3; %average activity
eps=.2; %averaging parameter
h=[.5 .5 .5 .5 -.2 .5 .3 .1 .5 .9 -.1 .7 .3 .1 -2 -1 -.2 -.5]';
%h=[-.2+.5*rand(1,45) .6-rand(1,15)*1.5]

% for i=1:length(a)
%     r(i)=max(eig(H0lf(n,f,m,a(i),eps)))-max(eig(H0lf(n,f,m,1,eps*a(i))));
% end
% plot(a,r);
sigma=0:0.01:0.3;
l = n-f;
q = eps*m/(n-1);

%We correct the vector h so that sigma is the stadard deviation
h(n)=-sum(h(1:n-1)); %h should sum to 0
h=sqrt(n)*h/sum(h.^2); %h should have 2-norm equal to sqrt(n)

%We compite the spectral radius of the unperturbed matrix and we use
%first-order perturbation to estimate the spectral radius of H, as s
%functon of the standard devition
[aa,bb]=eig(H0lf(n,f,m,a,eps));
A = eps*(f-1) + eps*(m-1)*(f-1)*(f-2)/(n-2) + q*(n-1)*(l-f+1) - q*a*l^2;
B = eps*(f-1) - 2*f + q*(n-1)^2 + q*a*(n+l-l^2);
C = q*a*(n + l) - 2;
g = ( -B + sqrt( B^2 - 4*A*C ) )/( 2*A );
A=(1+g*f)*(q-q^2*(n-1)*a)-g*q*n+g*q^2*n*(n-1)*a+q^2*a*(1+g*f)*(f-1)-q^2*n*a*(g*(f-1)+1);
C=q^2*(n-1)*(2*a-1)*(1+g*f)+(-q*n+q^2*n*(n-1)*a)-q^2*n*(n-1)*(2*a-1)*(1+g)+q*(n-1)*(n*q*a-1)+(1+g*f)*(f-1)*(eps*q*(m-1)/(n-2)-2*q^2*a)-(q-n*q^2*a)+g*(f-1)*(q^2*(n*(2*a-1)+1)-eps*q*(m-1)/(n-2))+(1+g)*(f-1)*(eps*q-eps*q*(m-1)/(n-2))+n*q^2*a;
rr=[g*(2*A*f+C)+2*A+C]/(f^2*g^2 + 2*f*g + f);
max(max(bb))
(1-l*q*a)^2+(2*q*a-(n+l)*q^2*a^2)/g



stima=bb(1,1)+sigma*rr*sum(h(1:f));
figure
plot(sigma/a,log(stima))
hold on

%We compute numerically the spectral radius, for different valuse of the
%standard deviation
for i=1:length(sigma)
    plot(sigma(i)/a,log(max(eig(Hlf(n,f,m,a,eps,h,sigma(i))))),'b*') 
end


plot([0 sigma(end)/a],log([max(eig(H0lf(n,f,m,1,eps*a))) max(eig(H0lf(n,f,m,1,eps*a)))]),'r--')