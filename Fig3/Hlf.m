function H=Hlf(n,f,m,a,eps,h,sigma)
% Computes the second order consensus matrix for a leader-follower
% consensus system with n agents, f followers, m links created by each node
% with activity equal to a+sigma*h and averaging parameter eps. The
% function receives in input a vector h of length f
if size(h,1)==1
    h=h';
end
if size(h,1)>f
    h=h(1:f);
end
    H=H0lf(n,f,m,a,eps)+sigma*H1lf(n,f,m,a,eps,h);
 for i=1:f
        for j=1:f
            if i~=j 
            H(Kro(i,j,j,i,f))=H(Kro(i,j,j,i,f))+eps^2*sigma^2*m^2*h(i)*h(j);
            end
        end
    end
    
    for i=1:f
        for j=1:f
            for k=1:f
                if i~=j && k~=j 
                H(Kro(i,j,k,i,f))=H(Kro(i,j,k,i,f))-eps^2*sigma^2*m^2*h(i)*h(j)/(n-1);
                end
            end
        end
    end
    
    
    for i=1:f
        for j=1:f
            for k=1:f
                if i~=j && k~=i 
                H(Kro(i,j,j,k,f))=H(Kro(i,j,k,i,f))-eps^2*sigma^2*m^2*h(i)*h(j)/(n-1);
                end
            end
        end
    end
    
    for i=1:f
        for j=1:f
            for k=1:f
                for p=1:f
                    if i~=j && k~=i && p~=j 
                    H(Kro(i,j,p,k,f))=H(Kro(i,j,p,k,f))+eps^2*sigma^2*m^2*h(i)*h(j)/(n-1)^2;
                    end
                end
            end
        end
    end
    
       
end