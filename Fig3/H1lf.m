function H=H1lf(n,f,m,a,eps,h)
% Computes the first order perturbation of the second order consensus matrix for a leader-follower
% consensus system with n agents, f followers, m links created by each node
% with activity equal to a+sigma*h and averaging parameter eps. The
% function receives in input a vector h of length f
if size(h,1)==1
    h=h';
end
if size(h,1)>f
    h=h(1:f);
end
q=m*eps/(n-1);
H=zeros(f^2);
    for i=1:f
        v=zeros(f,1);
        v(i)=1;
       % H((i-1)*f+1:f*i,(i-1)*f+1:f*i)=(eps*m/(n-1)-eps^2*m^2*a/(n-1))*(h*ones(1,f)-h(i)*v*ones(1,f)-diag(h)+h(i)*v*v')-eps^2*m^2*a*h(i)/(n-1)*(ones(f)-v*ones(1,f)-eye(f)+v*v')+(eps^2*m^2*a-eps*m)*(diag(h)+h(i)*eye(f)-2*h(i)*v*v')+(eps^2*m^2-2*eps*m)*h(i)*v*v'+(eps*m/(n-1)-eps^2*m^2/(n-1))*h(i)*(v*ones(1,f)-v*v');
        H((i-1)*f+1:f*i,(i-1)*f+1:f*i)=(q-q^2*(n-1)*a)*h*ones(1,f)+q^2*(n-1)*(2*a-1)*h(i)*v*ones(1,f)+(-q*n+q^2*n*(n-1)*a)*diag(h)+(-q^2*n*(n-1)*(2*a-1)*h(i))*v*v'-q^2*(n-1)*a*h(i)*ones(f)+(-q*(n-1)+q^2*n*(n-1)*a)*h(i)*eye(f);
    end
    for i=1:f
        for j=1:f
            if i~=j
                v=zeros(f,1);
                v(i)=1;
                u=zeros(f,1);
                u(j)=1;
                H((i-1)*f+1:f*i,(j-1)*f+1:f*j)=q^2*a*h(i)*ones(f)+q^2*a*h*ones(1,f)+(eps*q*(m-1)/(n-2)-2*q^2*a)*h(i)*v*ones(1,f)+(q-q^2*n*a)*h(i)*eye(f)+(-eps*q*(m-1)/(n-2)+q^2*(n*(2*a-1)+1))*h(i)*v*v'-q^2*n*a*diag(h)+(eps*q-eps*q*(m-1)/(n-2))*h(i)*v*u';
            end
        end
    end
end