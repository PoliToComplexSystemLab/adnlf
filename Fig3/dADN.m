function A = dADN(N,m,a)
%TVn(N,m,alpha) constructs an istance of a Time-varying direct network with N nodes, activation probabilities of the
%nodes given by vector alpha. Each node construcs m links.
A=zeros(N);
for i=1:N
    if rand<a(i)
        temp=modn(i+randperm(N-1),N);
        A(i,temp(1:m))=1;
    end       
end
end