function [ii,jj] = Kro(i,j,h,k,n)
%Maps the entry jh of the block ik of a n^2 kroneker product to its
%bidimensional position
ii=n*(j-1)+i;
jj=n*(h-1)+k;
end